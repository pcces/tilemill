FROM node:8.11.3-stretch

LABEL maintainer="dan@pcces.com.au"

RUN set -ex \
	&& apt-get update \
	&& apt-get install -y \
		git \
	&& rm -rf /var/lib/apt/lists/*

RUN set -ex \
	&& git clone https://github.com/tilemill-project/tilemill.git /tilemill

RUN set -ex \
	&& cd /tilemill \
	&& npm install

RUN set -ex \
	&& mkdir -p /etc/tilemill \
	&& mkdir -p /var/lib/tilemill \
	&& mkdir -p /var/log/tilemill \
	&& mkdir -p /var/lib/tilemill/project \
	&& mkdir -p /var/lib/tilemill/assets

COPY ./assets/config/config.json /etc/tilemill/config.json
COPY ./assets/fonts/* /tilemill/fonts/

WORKDIR /tilemill

EXPOSE 20008
EXPOSE 20009

CMD [ "./index.js", "--server=true", "--config=/etc/tilemill/config.json", "--debug=true", "--verbose" ]

# TileMill Docker Image

This Docker image provides a [TileMill](https://tilemill-project.github.io/tilemill/) Server.


# Usage

```sh
docker run -p 20008:20008 -p 20009:20009 pcces/tilemill:latest
```


# Notes

TileMill doesn't like to close with SIGINT, for some reason.

To stop a container using this image, find it's ID by using `docker ps`,
then stop it or kill it by using either `docker stop <id>` or `docker kill <id>`.


# Options

## Configuration File

TileMill is set to read configuration from `/etc/tilemill/config.json`.

If you want to provide your own configuration file, simply mount a file at
this location, or include your own file in a derivative docker image.


## Project Directory

The default configuration file lists the root tilemill directory as
`/var/lib/tilemill`.

To make your project available to the container, simply mount your project
directory *within* the `project` folder under the root tilemill folder.
For example:

```sh
docker run \
    -v $(pwd)/my-project:/var/lib/tilemill/project/my-project \
    -p 20008:20008 -p 20009:20009 \
    pcces/tilemill:latest
```

If you provide your own configuration file, you can specify the `"files"`
key in the JSON to change the projects directory to somewhere of your own
preference instead.


# Project Export

You can use this image to export a TileMill project by using `docker run` and
providing the necessary arguments.

The following example will export the project in the my-project folder in the
current path to the export folder in the current path, as an mbtiles file.
Note that the project name must be specified in the export command.

```
docker run \
	-v $(pwd)/my-project:/var/lib/tilemill/project/my-project \
	-v $(pwd)/export:/export:rw \
	tilemill \
		/tilemill/index.js \
		export \
		--config=/etc/tilemill/config.json \
		--format=mbtiles \
		my-project \
		/export/export.mbtiles
```


# Author

Daniel 'Vector' Kerr &lt;dan@pcces.com.au&gt; --- PCC Event Services


# License

This repository is released under the 3-Clause BSD License.

See the [LICENSE](LICENSE) file for details.


# Third Party

## Roboto Font Family

This image bundles the [Roboto font family](https://fonts.google.com/specimen/Roboto),
which falls under the [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0).
